1. Clone the repository, 
  
  git clone https://gitlab.eurecom.fr/oai/docker-image-oai5g.git

2. Load the repository (assuming docker is already installed)
  
  cd docker-image-oai5g
  
  docker load -i oai_5g.tar
  
3. Mysql root password <linux>